import {useState} from 'react'

import 'bootstrap/dist/css/bootstrap.min.css';
// import Popper from '@popperjs/core';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import 'bootstrap-icons/font/bootstrap-icons.css';
import './App.css';

import { useAlert } from "react-alert";

import NavBarComponent from './componentes/NavBarComponent';
import ProductsListComponent from './componentes/ProductsListComponent';

let products = []

function App() {  
  
  const [productsCart, setProductsCart] = useState([])
  const alert = useAlert();

  const handleProductsCart =(e, productAddCart)=>{
    
    products = [...products, productAddCart]
    setProductsCart(products)
    alert.success("Un Producto añadido al carrito de compras ");
      
  }

  const handlerDeleteOneProductCart = (e, productId)=>{

    //console.log('current products:', productsCart)
    products = productsCart.filter((prod) => {
      return parseInt(prod.id) !== parseInt(productId)
    })    
    setProductsCart(products)   
    //console.log('Delete one product')
    alert.show("Un producto quitado del carrito de compras");  
     
  }

  const handlerDeleteAllProductsCart = (e)=>{  
    setProductsCart([])   
    alert.error("Todos los productos fueron eliminados del carrito de compras");       
  }

  return (
    <>
        <NavBarComponent  handlerDeleteOneProductCart={handlerDeleteOneProductCart} handlerDeleteAllProductsCart={handlerDeleteAllProductsCart} productsCart={productsCart}></NavBarComponent>
        <br/>      
        <ProductsListComponent handleProductsCart={handleProductsCart} handlerDeleteOneProductCart={handlerDeleteOneProductCart} productsCart={productsCart }></ProductsListComponent>

    </>
  );

  
}

export default App;
