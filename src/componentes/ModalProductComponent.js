import React, { Component} from 'react';
import {Card, Modal, Container, Badge} from 'react-bootstrap'
import ButtonAddCartComponent from './ButtonAddCartComponent'
import LoaderComponent from './Loader/LoaderComponent';
import MessageComponent from './Loader/MessageComponent';


class ModalProductComponent extends Component {
    
    state = {
        productData : {},
        loading: false,
        error: null 
    }

    render() {
        return ( <Container>
            {(this.state.error)
            ? <MessageComponent msg={`Error ${this.state.error.status}: ${this.state.error.statusText}`} bgcolor="#dc3545" ></MessageComponent>
            : (this.state.loading && <LoaderComponent></LoaderComponent>)
            }
            <Card.Link onClick={this.props.handleShow} style={{ paddingRight: '1rem' }}><i className="bi bi-exclamation-circle"></i> Vista Previa</Card.Link>                                                
            <Modal show={this.props.show} onHide={this.props.handleClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>{this.state.productData.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Container>
                        <Card> 
                            <div className="text-center">
                                <Card.Img variant="top" src={this.state.productData.image} style={{height: "21rem", width:"21rem"}}/>
                            </div>
                            <Card.Body>
                                <h1 className="text-end " >
                                    <Badge bg="light" text="dark">S/. {this.state.productData.price}</Badge> 
                                </h1>
                                
                                
                                
                                <Card.Text>
                                    {this.state.productData.description}
                                </Card.Text>
                            </Card.Body>
                        </Card>                                   
                    </Container>

                </Modal.Body>
                <Modal.Footer>
                    <ButtonAddCartComponent 
                    addedInCart={this.props.addedInCart} 
                    id={this.props.id} 
                    product={this.props.product} 
                    handleProductsCart={this.props.handleProductsCart} 
                    handlerDeleteOneProductCart={this.props.handlerDeleteOneProductCart} 
                    productsCart={this.props.productsCart} ></ButtonAddCartComponent>

                </Modal.Footer>
            </Modal>              
        </Container>
        )
    }

    componentDidMount=(e)=> {
            
        let error = {}
        this.setState({ loading: true } )

        fetch('https://fakestoreapi.com/products/'+this.props.id)
            .then(res=>{
                //console.log('res:', res)
                if(res.ok){
                    return res.json()
                } else {
                    error = {
                        err: true,
                        status: res.status || "00",
                        statusText: res.statusText || "Ocurrio un error inesperado"
                    }
                    this.setState({error: error})                    
                    return  Promise.reject(error)
                }               
                
            }
            )
            .then(json=>{

                
                console.log('json:', json)
                this.setState({ productData: json, loading: false } )
            }).catch((err)=>{
                error = {
                    err: true,
                    status: err.status||' ',
                    statusText: err.statusText || "Ocurrio un error inesperado"
                }

                this.setState({error: error})  
                return err
            })        
    }
    

}

export default ModalProductComponent