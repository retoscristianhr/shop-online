import React, { Component} from 'react';
import {Navbar, Container, Badge, NavDropdown} from 'react-bootstrap'
import CardProductsComponent from './CardProductsComponent';

class NavBarComponent extends Component {
    
    state = {
      cardProducts:[]
    }

    render() {
      //className => justify-content-end
        return <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home">Tienda</Navbar.Brand>
          <Navbar.Toggle />
          <div className="col-md-9">

          </div>
            <Navbar.Collapse style={{  display: 'flex', justifyContent: 'space-around'}}>
              <Navbar.Text>
                  <Badge bg="success">{this.props.productsCart.length}  </Badge>  {' '}
              </Navbar.Text>
               
              
              <NavDropdown className="text-center" style={{maxWidth: '24rem', minWidth: '24rem'}} variant="success" title={<i className="bi bi-cart3"></i>} id="navbarScrollingDropdown">
                  <CardProductsComponent handlerDeleteOneProductCart={this.props.handlerDeleteOneProductCart} handlerDeleteAllProductsCart={this.props.handlerDeleteAllProductsCart} productsCart={this.props.productsCart}></CardProductsComponent>
                                   
                  <NavDropdown.Divider />
              </NavDropdown>
            </Navbar.Collapse>

        </Container>
      </Navbar>
    }

    
    
    
}

export default NavBarComponent