import React, { Component} from 'react';

import {ListGroup, Button, Badge, Container, NavDropdown} from 'react-bootstrap'

class CardProductsComponent extends Component{
    state={
        products:[],
        idDeleteProduct: null,
        costoTotal: 0,
    }

    

    constructor(props) {
        super(props)
        this.mostrar = this.mostrar.bind(this)
    }

    render(){
        let costoTotal = 0;
        return (
            <>
                <Container style={{maxWidth: '17rem', minWidth: '17rem', padding:'0.2rem'}}>
                    
                    <ListGroup as="ol" numbered style={{ padding:'0.1rem'}} >
                    <div className="container">
                        <div className="row">
                        <div className="col-md-6">
                            <h4 className="fw-bold">Carrito</h4>
                        </div >
                        <div className="col-md-6">
                            {(this.props.productsCart.length )
                            ? <Button onClick={this.props.handlerDeleteAllProductsCart} variant="warning" ><i className="bi bi-cart-plus"></i> Vaciar</Button>
                            : ' '
                            }
                            

                        </div>                     
                        </div>
                    </div >

                    {this.props.productsCart.map((el)=>{
 
                         costoTotal =costoTotal  +  this.trunc(el.price, 2)

                         return (
                            <ListGroup.Item key={el.id} as="li" style={{ padding:'0.3rem'}} >
                                <div className="row">
                                    <div> <img src={el.image} style={{height: "1.5rem", width:"1.5rem"}} alt={el.title} /> </div>
                                    <div className="col-md-7" style={{ paddingLeft:'0.4rem', paddingRight:'0.1rem'}}>
                                        {el.title}
                                    </div >
                                    <div className="col-md-3" style={{ paddingLeft:'0.4rem', paddingRight:'0.1rem'}}>                                
                                        <Badge variant="primary" pill>
                                            S/. {el.price}
                                        </Badge>
                                    </div>                     
                                    <div className="col-md-2"style={{ paddingLeft:'0.4rem', paddingRight:'0.1rem'}}>
                                    <a onClick={this.mostrar} id={el.id} className="link-danger" href="#home" ><i className="bi bi-trash" id={el.id}  ></i></a>
                                    </div>                     
                                </div>     
                            </ListGroup.Item> 
                        )                    
                    })}               
                    </ListGroup>
                    <div className="row">
                        <div className="col-md-4">
                            <p className="fs-3"> Total: {'  '} 
                                 
                            </p>
                        </div>
                        <div className="col-md-8">
                            <p className="fs-3">  
                                <span className="text-end"  > S/. {costoTotal}</span>                                
                            </p>

                        </div>
                        
                    </div>
                    <NavDropdown.Divider />
                </Container>            
            </>

        )
    }

    trunc = (x, posiciones = 0) => {
        var s = x.toString()
        var l = s.length
        var decimalLength = s.indexOf('.') + 1
      
        if (l - decimalLength <= posiciones){
          return x
        }
        
        var isNeg  = x < 0
        var decimal =  x % 1
        var entera  = isNeg ? Math.ceil(x) : Math.floor(x)
         var decimalFormated = Math.floor(
          Math.abs(decimal) * Math.pow(10, posiciones)
        )
        var finalNum = entera + 
          ((decimalFormated / Math.pow(10, posiciones))*(isNeg ? -1 : 1))
        
        return finalNum
      }
      
    // mostrar = (event)=>{
    mostrar(e){
        e.preventDefault();
        //e.stopPropagation();
        let id = e.target.getAttribute('id');

        this.props.handlerDeleteOneProductCart(e, id);

    }
}


export default CardProductsComponent;