import React, { Component} from 'react';
import { Button} from 'react-bootstrap'

class ButtonAddCartComponent extends Component {
    state = {
        addToCart: false,
        idProduct: 0
    }
  
    render() {
        return ( <>
            
            

            { (!this.props.addedInCart)?
                <Button onClick={this.clickAddCart} variant="success" ><i className="bi bi-cart-plus"></i> Agregar</Button>
                :<Button onClick={this.deleteProuctSelect} variant="danger" ><i className="bi bi-cart-x"></i>Eliminar</Button>
            
            }
        </>
        )
    }


    clickAddCart = (e) =>{

        this.setState({addToCart: !this.state.addToCart});               
        this.props.handleProductsCart(e, this.props.product)
    }

    deleteProuctSelect = (e)=>{
        //console.log('id: ', this.props.id)
        this.setState({addToCart: !this.state.addToCart});
        this.props.handlerDeleteOneProductCart(e, this.props.id)
    }

}

export default ButtonAddCartComponent