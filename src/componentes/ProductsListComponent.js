import React, { Component} from 'react';
import { Row, Container} from 'react-bootstrap'

import ProductComponent from './ProductComponent';
import LoaderComponent from './Loader/LoaderComponent';
import MessageComponent from './Loader/MessageComponent';

class ProductsListComponent extends Component {
   
    state = {
        products: [],
        loading: true,
        error: null
    }

    render() {
        return (

        <Container className="text-center">
           
            {(this.state.error)
            ? <MessageComponent msg={`Error ${this.state.error.status}: ${this.state.error.statusText}`} bgcolor="#dc3545" ></MessageComponent>
            : (this.state.loading && <LoaderComponent></LoaderComponent>)
            }
            
            <Row xs={1} md={4} className="g-4">
                {this.state.products.map((el)=>{
                    
                    return <ProductComponent 
                        productsCart={this.props.productsCart}
                        handleProductsCart={this.props.handleProductsCart} 
                        handlerDeleteOneProductCart={this.props.handlerDeleteOneProductCart} 
                        key={el.id}  
                        id={el.id}  
                        title={el.title} 
                        category={el.category} 
                        description={el.description}  
                        price={el.price} 
                        rating={el.rating} 
                        image={el.image}/>
                })
                }
            </Row>                     
        </Container>
        )
    }

    componentDidMount() {
        let error = {}

        this.setState({ loading: true } )

        fetch('https://fakestoreapi.com/products')
            .then(res=>{
                //console.log('res:', res)
                if(res.ok){
                    return res.json()
                } else {

                    error = {
                        err: true,
                        status: res.status || "00",
                        statusText: res.statusText || "Ocurrio un error inesperado"
                    }
                    this.setState({error: error})                    
                    return  Promise.reject(error)
                }
                
                
            }
            )
            .then(json=>{

                json.forEach(el=>{
                    
                    fetch(el.image)
                    .then((resp)=>resp.url)                    
                    .then((js)=>{
                       
                        let product = {
                                id: el.id,
                                title: el.title,
                                category: el.category,
                                description: el.description,
                                price: el.price,
                                rating: el.rating,
                                image: el.image,
                            }

                        let products = [...this.state.products, product]
                        this.setState({products})
                        this.setState({ loading: false } )

                    })
                })
            }).catch((err)=>{
                error = {
                    err: true,
                    status: err.status||' ',
                    statusText: err.statusText || "Ocurrio un error inesperado"
                }

                this.setState({error: error})  
                return err
            })        
    }

   
}

export default ProductsListComponent