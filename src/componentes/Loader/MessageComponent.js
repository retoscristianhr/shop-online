import { Component} from 'react';

import './LoaderComponent.css'

class MessageComponent extends Component {

    styles = { 
            padding: '1rem',
            marginBottom: '1rem',
            textAlign: 'center',
            color: '#fff',
            fontWeight: 'bold',
            backgroundColor: "#dc3545" 
        } 
  
        
    render() {
        return (
        <div style={this.styles}>
            <p>{this.props.msg}</p>
        </div>
        )
    }

    componentDidMount() {

    }
}

export default MessageComponent