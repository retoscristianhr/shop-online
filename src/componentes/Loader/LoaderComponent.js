import { Component} from 'react';

import './LoaderComponent.css'

class LoaderComponent extends Component {

    render() {
        return (
        <div className="lds-loading">
            <div></div>
            <div></div>
            <div></div>
        </div>
        )
    }
}

export default LoaderComponent