import React, { useState} from 'react';
import {Card, Col, Badge} from 'react-bootstrap'


import ButtonAddCartComponent from './ButtonAddCartComponent'
import ModalProductComponent from './ModalProductComponent';

function ProductComponent(props)  {
   
    const [show, setShow] = useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    
    let product = props.productsCart.find(product =>{
                    return (parseInt(product.id) === parseInt(props.id))            
                });

    let addedInCart =  (product === undefined)?false:true;
 
    return (
    
        <Col key={props.id}>
            
            
            <Card >
            <Card.Img variant="top" className="rounded mx-auto d-block" style={{height: "8rem", width:"8rem"}} src={props.image} />
            <Card.Body >
                <Card.Title>{props.title.length>39?props.title.slice(0, 39)+'...':props.title}</Card.Title>
                
                <Card.Text>
                    {(props.description.length>81)?props.description.slice(0, 79)+"....": props.description}
                    <br></br>
                    <li style={{listStyle: 'none', textAlign: 'right', fontSize: '25px'}} >
                        <Badge bg="light" text="dark">S/. {props.price}</Badge> 
                    </li>
                    
                </Card.Text>
                <ModalProductComponent 
                show={show} 
                handleClose={handleClose} 
                handleShow={handleShow}
                addedInCart={addedInCart} 
                id={props.id} 
                product={props} 
                handleProductsCart={props.handleProductsCart} 
                handlerDeleteOneProductCart={props.handlerDeleteOneProductCart} 
                productsCart={props.productsCart} 
                ></ModalProductComponent>
                

                <ButtonAddCartComponent 
                addedInCart={addedInCart} 
                id={props.id} 
                product={props} 
                handleProductsCart={props.handleProductsCart} 
                handlerDeleteOneProductCart={props.handlerDeleteOneProductCart} 
                productsCart={props.productsCart} ></ButtonAddCartComponent>
            </Card.Body>
            </Card>
        </Col>
            
    )
    

    
    
}

export default ProductComponent